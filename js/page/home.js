//Home Banner
var homeBanner = new MasterSlider();
homeBanner.setup('home-banner', {
    width: 1024,
    height: 768,
    space: 5,
    view: 'parallaxMask',
    layout: 'fullscreen',
    speed: 20
});
homeBanner.control('bullets', {autohide: false});
// add scroll parallax effect
MSScrollParallax.setup(homeBanner, 50, 80, true);

//Rooms Selection
var slides = $('.rooms-thumb').length; //calculate no. slides to show

$('#rooms-selection-list').slick({//Vertical Room Selection Slider
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    swipe: false,
    dots: false,
    vertical: false,
    focusOnSelect: true,
    asNavFor: '#rooms-slide',
    infinite: false,
    mobileFirst: true,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 1023,
            settings: {
                arrows: false,
                slidesToShow: slides,
                vertical: true
            }
        }
    ]
});

$('#rooms-slide').slick({//Selected room slider
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    swipe: false,
    fade: true,
    asNavFor: '#rooms-selection-list'
});

$('.selected-room-slide').slick({//Selected room images slides
    adaptiveHeight: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    focusOnSelect: true,
    infinite: false
});

//Scrollable init
var windowWidth = $(window).width();
if (windowWidth >= 1024) {
    $('.scroll-vertical').scrollable({
        autoHide: false
    });
}